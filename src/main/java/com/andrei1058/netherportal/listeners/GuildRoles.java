package com.andrei1058.netherportal.listeners;

import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberRoleAddEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberRoleRemoveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import static com.andrei1058.netherportal.NetherPortal.getDatabase;

public class GuildRoles extends ListenerAdapter {

    @Override
    public void onGuildMemberRoleAdd(GuildMemberRoleAddEvent e) {
        getDatabase().updateUserRanks(e.getGuild().getId(), e.getMember());
    }

    @Override
    public void onGuildMemberRoleRemove(GuildMemberRoleRemoveEvent e) {
        getDatabase().updateUserRanks(e.getGuild().getId(), e.getMember());
    }

    @Override
    public void onGuildMemberLeave(GuildMemberLeaveEvent e) {
        if (!e.getMember().getRoles().isEmpty()){
            getDatabase().updateUserRanks(e.getGuild().getId(), e.getMember());
        }
    }
}
