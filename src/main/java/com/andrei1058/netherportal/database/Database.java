package com.andrei1058.netherportal.database;

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.andrei1058.netherportal.NetherPortal.getConfig;
import static com.andrei1058.netherportal.NetherPortal.getJda;

public class Database {

    private Connection connection;


    public Database() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }

        if (!connect()) {
            System.out.println("Can't connect to database!");
            if (getJda() != null) {
                getJda().shutdownNow();
            }
            System.exit(1);
            return;
        }
    }

    public boolean connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + getConfig().getYml().getString("database.host")
                    + ":" + getConfig().getYml().getInt("database.port")
                    + "/" + getConfig().getYml().getString("database.name")
                    + "?autoReconnect=true&user=" + getConfig().getYml().getString("database.user")
                    + "&password=" + getConfig().getYml().getString("database.pass")
                    + "&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void close() {
        if (!isConnected()) return;
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createRankedTable(String guildId) {
        if (!isConnected()) connect();
        try {
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS guild_ranked_" + guildId + " (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                    " user_id BIGINT, ranks MEDIUMBLOB);");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isRankedTableExist(String guildId) {
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT 1 FROM guild_ranked_" + guildId + " LIMIT 1;");
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isUserRanked(long userId, String serverId) {
        if (!isRankedTableExist(serverId)) {
            createRankedTable(serverId);
        }
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT ranks FROM guild_ranked_" + serverId + " WHERE user_id = " + userId + ";");
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Long> getUserRanks(String serverId, long userId) {
        if (!isRankedTableExist(serverId)) {
            createRankedTable(serverId);
        }
        List<Long> ranks = new ArrayList<>();
        if (!isConnected()) connect();
        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT ranks FROM guild_ranked_" + serverId + " WHERE user_id = " + userId + ";");
            if (rs.next()) {
                for (String s : rs.getString("ranks").split("-")) {
                    ranks.add(Long.valueOf(s));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ranks;
    }

    public void updateUserRanks(String serverId, Member member) {
        if (!isRankedTableExist(serverId)) {
            createRankedTable(serverId);
        }
        String ranks = "";
        for (Role r : member.getRoles()) {
            ranks += r.getId() + "-";
        }

        if (isUserRanked(member.getUser().getIdLong(), serverId)) {
            if (!isConnected()) connect();
            try {
                PreparedStatement ps = connection.prepareStatement("UPDATE guild_ranked_" + serverId + " SET ranks=? WHERE " +
                        "user_id=" + member.getUser().getIdLong() + ";");
                ps.setString(1, ranks);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return;
        }

        if (!isConnected()) connect();
        try {
            connection.createStatement().execute("INSERT INTO guild_ranked_" + serverId + " (user_id, ranks) " +
                    "VALUES (" + member.getUser().getIdLong() + "," + ranks + ");");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public boolean isConnected() {
        return connection != null;
    }
}
