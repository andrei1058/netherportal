package com.andrei1058.netherportal;

import com.andrei1058.netherportal.configuration.ConfigManager;
import com.andrei1058.netherportal.database.Database;
import com.andrei1058.netherportal.listeners.GuildJoin;
import com.andrei1058.netherportal.listeners.GuildRoles;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

import javax.security.auth.login.LoginException;

public class NetherPortal {

    private static JDA jda;
    private static ConfigManager config;
    private static Database database;

    public static void main(String... args){
        setupConfig();
        if (config.getYml().getString("token").isEmpty()){
            System.out.println("Please set up your bot token!");
            return;
        }

        database = new Database();

        try {
            jda = new JDABuilder(config.getYml().getString("token")).addEventListener(new GuildJoin(), new GuildRoles()).build();
        } catch (LoginException e) {
            e.printStackTrace();
            return;
        }

    }

    private static void setupConfig(){
        config = new ConfigManager("config", "NetherPortal");
        config.addDefault("token", "");
        config.addDefault("database.host", "localhost");
        config.addDefault("database.port", 3306);
        config.addDefault("database.name", "NetherPortal");
        config.addDefault("database.user", "andrei1058");
        config.addDefault("database.pass", "bread");
        config.saveDefaults();
    }

    public static ConfigManager getConfig() {
        return config;
    }

    public static JDA getJda() {
        return jda;
    }

    public static Database getDatabase() {
        return database;
    }
}
